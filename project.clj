(defproject bongari "0.1.0-SNAPSHOT"
            :description "FIXME: write description"
            :url "http://example.com/FIXME"
            :license {:name "Eclipse Public License"
                      :url  "http://www.eclipse.org/legal/epl-v10.html"}

            :source-paths ["src/clj" "src/cljs"]

            :dependencies [[org.clojure/clojure "1.8.0"]
                           [ring-server "0.4.0"]
                           [reagent "0.6.0-rc"]
                           [reagent-utils "0.1.9"]
                           [ring "1.5.0"]
                           [ring/ring-defaults "0.2.1"]
                           [prone "1.1.1"]
                           [compojure "1.5.1"]
                           [hiccup "1.0.5"]
                           [environ "1.0.3"]
                           [enlive "1.1.6"]
                           [cljs-http "0.1.41"]
                           [org.clojure/clojurescript "1.9.93" :scope "provided"]
                           [http-kit "2.1.18"]
                           [secretary "1.2.3"]]

            :plugins [[lein-ring "0.9.7"]
                      [lein-environ "1.0.3"]
                      [lein-asset-minifier "0.3.0"]]

            :ring {:handler      bongari.handler/app
                   :uberwar-name "bongari.war"}

            :min-lein-version "2.5.0"

            :uberjar-name "bongari.jar"

            :main bongari.server

            :clean-targets ^{:protect false} ["resources/public/js"]

            :minify-assets
            {:assets
             {"resources/public/css/site.min.css" "resources/public/css/site.css"}}

            :cljsbuild {:builds {:app {:source-paths ["src/cljs"]
                                       :compiler     {:output-to     "resources/public/js/app.js"
                                                      :output-dir    "resources/public/js/out"
                                                      :asset-path    "js/out"
                                                      :optimizations :none
                                                      :pretty-print  true}}}}

            :profiles {:dev     {:repl-options {:init-ns          bongari.repl
                                                :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}

                                 :dependencies [[ring-mock "0.1.5"]
                                                [ring/ring-devel "1.5.0"]
                                                [weasel "0.7.0"]
                                                [leiningen-core "2.6.1"]
                                                [com.cemerick/piggieback "0.2.1"]
                                                [org.clojure/tools.nrepl "0.2.12"]

                                                [pjstadig/humane-test-output "0.8.0"]]

                                 :source-paths ["env/dev/clj"]
                                 :plugins      [[lein-figwheel "0.5.4-7"]
                                                [lein-cljsbuild "1.1.3"]]

                                 :injections   [(require 'pjstadig.humane-test-output)
                                                (pjstadig.humane-test-output/activate!)]

                                 :figwheel     {:http-server-root "public"
                                                :server-port      3449
                                                :css-dirs         ["resources/public/css"]
                                                :ring-handler     bongari.handler/app}

                                 :env          {:dev true}

                                 :cljsbuild    {:builds {:app {:source-paths ["env/dev/cljs"]
                                                               :compiler     {:main       "bongari.dev"
                                                                              :source-map true}}}}}



                       :uberjar {:hooks       [leiningen.cljsbuild minify-assets.plugin/hooks]
                                 :env         {:production true}
                                 :aot         :all
                                 :omit-source true
                                 :cljsbuild   {:jar    true
                                               :builds {:app
                                                        {:source-paths ["env/prod/cljs"]
                                                         :compiler
                                                                       {:optimizations :simple
                                                                        :pretty-print  false}}}}}})
