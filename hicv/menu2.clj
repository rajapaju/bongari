([:html
  [:body
   [:div.navbar.navbar-inverse.navbar-fixed-top
    [:div.navbar-inner
     [:div.container
      [:button.btn.btn-navbar.collapsed
       {:data-target ".nav-collapse",
        :data-toggle "collapse",
        :type "button"}
       [:span.icon-bar]
       [:span.icon-bar]
       [:span.icon-bar]]
      [:a.brand {:href "#"} "Project name"]
      [:div.nav-collapse.collapse
       {:style "height: 0px;"}
       [:ul.nav
        [:li.active [:a {:href "#"} "Home"]]
        [:li [:a {:href "#about"} "About"]]
        [:li [:a {:href "#contact"} "Contact"]]]]
      "<!--/.nav-collapse -->"]]]]])
