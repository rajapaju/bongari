[:div.container
 [:form.form-signin
  [:h2.form-signin-heading "Please sign in"]
  [:label.sr-only {:for "inputEmail"} "Username"]
  [:input#inputEmail.form-control
   {:autofocus "",
    :required "",
    :placeholder "Email address",
    :type "email"}]
  [:label.sr-only {:for "inputPassword"} "Password"]
  [:input#inputPassword.form-control
   {:required "", :placeholder "Password", :type "password"}]
  [:div.checkbox
   [:label
    [:input {:value "remember-me", :type "checkbox"}]
    " Remember me\n            "]]
  [:button.btn.btn-lg.btn-primary.btn-block
   {:type "submit"}
   "Sign in"]]]
