([:nav.navbar.navbar-inverse.navbar-fixed-top
  [:div.container
   [:div.navbar-header
    [:button.navbar-toggle.collapsed
     {:aria-controls "navbar",
      :aria-expanded "false",
      :data-target "#navbar",
      :data-toggle "collapse",
      :type "button"}
     [:span.sr-only "Toggle navigation"]
     [:span.icon-bar]
     [:span.icon-bar]
     [:span.icon-bar]]
    [:a.navbar-brand {:href "#"} "Project name"]]
   [:div#navbar.collapse.navbar-collapse
    [:ul.nav.navbar-nav
     [:li.active [:a {:href "#"} "Home"]]
     [:li [:a {:href "#about"} "About"]]
     [:li [:a {:href "#contact"} "Contact"]]]]
   "<!--/.nav-collapse -->"]])
