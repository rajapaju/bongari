(ns bongari.utils
  (:require
    [goog.history.EventType :as EventType]
    [goog.events :as events]
    [secretary.core :as secretary :include-macros true])
  (:import goog.History))


(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
      EventType/NAVIGATE
      (fn [event]
        (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn log [& args]
  (.log js/console (apply str args)))


;(defn start-timer []
;  (state/put! :time (state/get-in [:settings :time-in-seconds]))
;  (state/put! :score 0)
;  (go
;    (while (pos? (state/get :time))
;      (do
;        (<! (timeout 1000))
;        (if  (pos? (state/get :time))
;          (state/update-in! [:time] dec))))
;    (let [score (state/get :score) top-score (state/get :top-score)]
;      (if (< top-score score )
;        (state/put! :top-score score)))))