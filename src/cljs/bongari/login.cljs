(ns bongari.login
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [reagent.core :as r]
    [cljs.core.async :refer [put! chan <! >! buffer]]))


; https://developers.google.com/identity/sign-in/web/reference#gapiauth2googleauth


(enable-console-print!)

(def user (r/atom {}))

(defn- load-gapi-auth2 []
  (let [c (chan)]
    (.load js/gapi "auth2" #(go (>! c true)))
    c))

(defn- auth-instance []
  (.getAuthInstance js/gapi.auth2))

(defn- get-google-token []
  (-> (auth-instance) .-currentUser .get .getAuthResponse .-id_token))

(defn- handle-user-change [u]
  (let [profile (.getBasicProfile u)]
    (reset! user
            {:name       (if profile (.getName profile) nil)
             :image-url  (if profile (.getImageUrl profile) nil)
             :token      (get-google-token)
             :signed-in? (.isSignedIn u)})))

(defonce _ (go
             (<! (load-gapi-auth2))
             (.init js/gapi.auth2
                    (clj->js {"client_id" "47315704837-ip6n037bm5gohq7394congdgtbmg15fo.apps.googleusercontent.com"
                              "scope"     "profile"}))
             (let [current-user (.-currentUser (auth-instance))]
               (.listen current-user handle-user-change))))


(defn sign-out []
  (.signOut (auth-instance)))

(defn sign-in []
  (.signIn (auth-instance)))

(defn signed-in? []
  (:signed-in? @user))


