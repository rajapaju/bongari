(ns bongari.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [cljs-http.client :as http]
            [cljs.core.async :as async :refer [>! <! timeout put! chan alts!]]
            [secretary.core :as secretary :include-macros true]
            [bongari.utils :as utils]
            [bongari.login :as login]))

;; -------------------------
;; Views
(session/reset! {:all-answers          []
                 :current-game-answers []
                 :current-answer       nil
                 :picture-size         200
                 :current-score        0
                 :high-score           {}
                 :miss                 3
                 :lives                3
                 :game-answer-count    6
                 :current-game-id      nil
                 :games-list           nil})

(defn live-icon []
  [:i {:class "fa fa-heart" :style {:color "red"}}])

(defn dead-icon []
  [:i {:class "fa fa-heart-o" :style {:color "red"}}])


(defn corrent [b]
  (let [game-birds (take (session/get :game-answer-count) (shuffle (session/get :all-answers)))]
    (session/put! :current-game-answers game-birds)
    (session/put! :current-answer (rand-nth game-birds))
    (session/update-in! [:current-score] inc)
    (if (< (session/get-in [:high-score (session/get :current-game-id)]) (session/get :current-score))
      (session/update-in! [:high-score (session/get :current-game-id)] #(session/get :current-score)))))

(defn miss [b]
  (let [game-birds (take (session/get :game-answer-count) (shuffle (session/get :all-answers)))]
    (session/put! :current-game-answers game-birds)
    (session/put! :current-answer (rand-nth game-birds))
    (session/update-in! [:miss] dec)))


(defn bird-item [b]
  [:div.col.s12.m6.l2
   [:div.card {:on-click #(if (= b (session/get :current-answer))
                           (corrent b)
                           (miss b))}
    [:div.card-image
     [:img {:src (:url b)}]
     [:span.card-title.small (get-in b [:name :la])]]]])



(defn new-game []
  (session/remove! :current-answer)
  (let [game-birds (take (session/get :game-answer-count) (shuffle (session/get :all-answers)))]
    (session/put! :current-game-answers game-birds)
    (session/put! :current-answer (rand-nth game-birds)))
  (session/put! :miss (session/get :lives))
  (session/put! :current-score 0))

(defn game-page []
  [:div#main-container.conteiner
   (if (= 0 (session/get :miss))
     (do
       [:div.row
        [:div.col.s12.center-align
         [:a.waves-effect.waves-light.btn-large.blue-grey.lighten-1 {:on-click #(new-game)}
          "Uusi peli"]]])
     [:div.row
      (for [b (session/get :current-game-answers)]
        ^{:key (:fi (:name b))} [bird-item b])])])

(defn login-page []
  [:div.container.main-container
   [:div.row {:on-click #(login/sign-in)}
    [:div.col.s12.center-align
     [:a.waves-effect.waves-light.btn-large.blue-grey.lighten-1
      [:i.material-icons.left "power_settings_new"] "Sign in with Google"]]]])

(defn select-game [game]
  (secretary/dispatch! (str "/games/" (:id game))))

(defn games-list-page []
  [:ul.collection
   (for [game (session/get :games-list)]
     ^{:key (:id game)}
     [:li.collection-item.center
      [:div {:on-click (fn [e]
                         (do
                           (select-game game)
                           (.preventDefault e)))}
       (get-in game [:name :fi])
       [:a {:href "#!", :class "secondary-content"}
        [:i {:class "material-icons"} "send"]]]])
   ;[:li.collection-item.center
   ; [:div {:on-click (fn [e]
   ;                    (secretary/dispatch! (str "/taxonomy/MX.37600")))}
   ;  "Selaa lajeja"
   ;  [:a {:href "#!", :class "secondary-content"}
   ;   [:i {:class "material-icons"} "send"]]]]
   ])

(defn get-name [item]
  (if-let [fi-name (:fi (:vernacularName item))]
    fi-name
    (:scientificName item)))


(defn update-breadcrumb [item]
  (let [breadcrumb (session/get :breadcrumb [])]
    (prn (type breadcrumb))
    (if (some #{item} breadcrumb)
      (session/put! :breadcrumb (conj (vec (take-while #(not= (:qname item) (:qname %)) breadcrumb)) item))
      (session/put! :breadcrumb (vec (conj breadcrumb item))))
    (prn (map :qname breadcrumb))))

(defn breadcrumb []
  [:div (for [crumb (session/get :breadcrumb)]
          (let [name (get-name crumb)]
            ^{:key (:qname crumb)}
            [:a {:href "#" :on-click #((secretary/dispatch! (str "/taxonomy/" (:qname crumb)))
                                       (.preventDefault %))}
             (str " / " name)]))])

(defn taxonomy-browser-page []
  (let [item (session/get :taxonomy-item)
        has-children (:hasChildren item)]
    [:div.center
     (breadcrumb)
     (if-not has-children
       [:img.center {:src (:fullURL (first (:multimedia item))), :alt "", :class "responsive-img"}])
     (if (:children item)
       (for [child (:children item)]
         (let [name (get-name child)
               q-name (:qname child)
               has-children (:hasChildren child)]
           ^{:key (:qname child)} [:div
                                   [:a {:href "#" :on-click (fn [e] (do
                                                                      (secretary/dispatch! (str "/taxonomy/" q-name))
                                                                      (.preventDefault e)))}
                                    (str name " " (:countOfFinnishSpecies child))
                                    ]])))]
    ))

(defn actions []
  [:div {:class "fixed-action-btn horizontal click-to-toggle" :styel {:bottom "45px" :right "24px"}}
   [:a {:class "btn-floating btn-large red"}
    [:i {:class "material-icons"} "menu"]]
   [:ul
    [:li
     [:a {:class "btn-floating red"}
      [:i {:class "material-icons"} "insert_chart"]]]
    [:li
     [:a {:class "btn-floating yellow darken-1"}
      [:i {:class "material-icons"} "format_quote"]]]
    [:li
     [:a {:class "btn-floating green"}
      [:i {:class "material-icons"} "publish"]]]
    [:li
     [:a {:class "btn-floating blue"}
      [:i {:class "material-icons"} "attach_file"]]]]])

(defn footer []
  [:footer.page-footer.light-blue.darken-1
   [:div.container [:div.row]]
   [:div.footer-copyright.light-blue.darken-2
    [:div.container
     [:div.col.s12.center-align
      "2016 Copyright Rapat"]]]])

(defn nav []
  (let [current-question (session/get-in [:current-answer :name :fi])]
    [:div.navbar-fixed
     [actions]
     [:nav
      [:div.nav-wrapper.light-blue.darken-2
       [:img.left {:src (:image-url @login/user), :alt "", :class "circle responsive-img"}]
       [:a.brand-logo.left {:href "#" :on-click (fn [e] (do
                                                          (secretary/dispatch! "/")
                                                          (.preventDefault e)))} "Bongari"]
       (if (and (not current-question) (login/signed-in?))
         [:a.right {:href "#" :on-click (fn [e] (do
                                                  (session/remove! :current-answer)
                                                  (login/sign-out)
                                                  (.preventDefault e)))} "Kirjaudu ulos"])

       (if current-question
         [:div.center current-question])]
      (if current-question
        [:a.right {:class "btn-floating btn-large waves-effect waves-light red right"}
         [:i (session/get :current-score)]])
      (if current-question
        [:div.right
         (for [icon (take (- (session/get :lives) (session/get :miss)) (repeat dead-icon))]
           [icon])
         (for [icon (take (session/get :miss) (repeat live-icon))]
           [icon])])
      ]]))

(defn current-page []
  (if (:signed-in? @login/user)
    [(session/get :current-page)]
    [login-page]))


;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")


(secretary/defroute "/" []
                    (session/remove! :current-answer)
                    (if-not (session/get :games-list)
                      (go
                        (let [response (:body (<! (http/get (str "data/games.json"))))]
                          (session/put! :games-list response))))
                    (session/put! :current-page #'games-list-page))

(secretary/defroute "/games/:id" [id]
                    (session/remove! :current-answer)
                    (if (:signed-in? @login/user)
                      (do
                        (session/put! :current-page #'game-page)
                        (session/put! :current-game-id id)
                        (go
                          (let [response (:body (<! (http/get (str "data/games/" id ".json"))))]
                            (session/put! :all-answers response)
                            (new-game))))

                      (session/put! :current-page #'login-page)))

(secretary/defroute "/taxonomy/:q-name" [q-name]
                    (go
                      (println q-name)
                      (let [response (:body (<! (http/get (str "taxonomy/" q-name ".edn"))))]
                        (session/put! :taxonomy-item response)
                        (update-breadcrumb response)
                        (if (:hasChildren response)
                          (let [children (:body (<! (http/get (str "taxonomy/" q-name "-children.edn"))))]
                            (session/put! :taxonomy-item (assoc response :children children))))
                        (session/put! :current-page #'taxonomy-browser-page))))

(defn mount-root []
  (reagent/render [nav] (.getElementById js/document "navbar"))
  (reagent/render [current-page] (.getElementById js/document "app"))
  (reagent/render [footer] (.getElementById js/document "footer")))

(defn init! []
  (session/put! :page :main-menu)
  (bongari.utils/hook-browser-navigation!)
  (mount-root))

(init!)
