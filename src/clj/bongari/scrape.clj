(ns bongari.scrape
  (:require [net.cgrand.enlive-html :as html]))

(def ^:dynamic *base-url* "http://atlas3.lintuatlas.fi/tulokset/lajit")

(defn fetch-url [url]
  (html/html-resource (java.net.URL. url)))

(defn select-rows [page]
  (html/select page [:table#speciestable :td]))

(defn testi []
  (->> *base-url*
       fetch-url
       select-rows
       rest))
