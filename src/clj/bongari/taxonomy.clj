(ns bongari.taxonomy
  (:require [org.httpkit.client :as http]
            [clojure.data.json :as json]
            [clojure.core.async
             :as a
             :refer [>! <! >!! <!! go chan buffer close! thread go-loop
                     alts! alts!! timeout]]))

;;https://beta.laji.fi/api/taxonomy/MX.53761/children?lang=fi
(def q-name "MX.37600")
(def url "http://laji.fi/api/taxonomy/")


(def options {:timeout      50000                           ; ms
              ;:basic-auth   ["" ""]
              :query-params {:lang         "fi"
                             :multimedia   true
                             :descriptions true}
              })

(defn async-get [url]
  (println url)
  (let [channel (chan)]
    (http/get url options #(go (>! channel %)))
    channel))

(def download-channel (chan))


(defn read-file [q-name]
  (let [file-name (str "/Users/ville/src/bongari/resources/public/taxonomy/" q-name ".edn")]
    (slurp file-name)))

(println (read-file "MX.37600"))
(defn write-to-file [name data]
  (let [file-name (str "/Users/ville/src/bongari/resources/public/taxonomy/" name ".edn")]
    (spit file-name data)))

(defn download-children [item]
  (go
    (let [q-name (:qname item)
          url (str url q-name "/children")
          children (-> url
                       (async-get)
                       (<!)
                       (:body)
                       (json/read-str :key-fn keyword)
                       (:results))]
      (write-to-file (str q-name "-children") children)
      (doseq [child children]
        (<! (timeout 100))
        (>! download-channel child)))))

;(go-loop [chan item-chan]
;         (let [item (<! chan)]
;           (if (:hasChildren item)
;             (doanload-chidren item))
;           (save-to-file item))
;         (recur chan))


(defn fetch-and-save [url q-name]
  (let [full-url (str url q-name)]
    (http/get full-url
              options
              (fn [{:keys [status headers body error]}]     ;; asynchronous response handling
                (if error
                  (println (str "Failed " full-url " ->" error))
                  (let [data (json/read-str body
                                            :key-fn keyword)]
                    (write-to-file q-name data)
                    (println (get-in data [:vernacularName :fi] "-") " | " (:scientificName data))
                    (if (:hasChildren data)
                      (download-children data))))))))


(go-loop []
  (fetch-and-save url (:qname (<! download-channel)))
  (<! (timeout 1000))
  (recur))

(comment
  (go
    (>! download-channel {:qname "MX.37600"})))





