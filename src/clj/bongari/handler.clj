(ns bongari.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources files]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [hiccup.core :refer [html]]
            [hiccup.page :refer [include-js include-css]]
            [prone.middleware :refer [wrap-exceptions]]
            [environ.core :refer [env]]))

(def home-page
  (html
    [:html
     [:head
      [:meta {:charset "utf-8"}]
      [:meta {:name    "viewport"
              :content "width=device-width, initial-scale=1"}]

      (include-js "https://apis.google.com/js/platform.js")
      (include-js "https://code.jquery.com/jquery-2.1.1.min.js")
      (include-css "http://fonts.googleapis.com/icon?family=Material+Icons")
      (include-css "http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css")
      (include-css "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css")
      (include-js "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js")
      (include-css (if (env :dev) "css/site.css" "css/site.min.css"))]

     [:body
      [:div#navbar]
      [:main [:div#app]]
      [:div#footer]
      (include-js "js/app.js")]]))

(defroutes routes
           (GET "/" [] home-page)
           (files "/data" {:root "./data"})
           (resources "/")
           (not-found "Not Found"))

(def app
  (let [handler (wrap-defaults routes site-defaults)]
    (if (env :dev) (wrap-exceptions handler) handler)))
